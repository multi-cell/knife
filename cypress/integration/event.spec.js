/* global context beforeEach cy it expect */

context('Event page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/event')
  })

  it('should say Hello World', () => {
    // https://on.cypress.io/each
    cy.get('.title')
      .should('contain', 'Hello World')
  })
})
