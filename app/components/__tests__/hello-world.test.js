/* global describe it expect */
import wait from 'waait'
import renderer from 'react-test-renderer'
import { MockedProvider } from 'react-apollo/test-utils'

import { HelloWorld, HELLO_WORLD as query } from '../hello-world'

const mocks = [
  {
    request: {
      query
    },
    result: {
      data: {
        hello: 'Hello World'
      }
    }
  },
  {
    request: {
      query,
      variables: {
        name: 'error'
      }
    },
    error: new Error('sorry bub')
  }
]

describe('HelloWorld component', () => {
  it('renders a response correctly', async () => {
    const tree = renderer.create(
      <MockedProvider mocks={mocks} addTypename={false}>
        <HelloWorld />
      </MockedProvider>
    )

    await wait(0)

    expect(tree).toMatchSnapshot()
  })

  it('renders an error', async () => {
    const tree = renderer.create(
      <MockedProvider mocks={mocks}>
        <HelloWorld name='error' />
      </MockedProvider>
    )

    await wait(0)

    expect(tree).toMatchSnapshot()
  })

  it('displays a loading state', () => {
    const tree = renderer.create(
      <MockedProvider mocks={mocks}>
        <HelloWorld />
      </MockedProvider>
    )

    expect(tree).toMatchSnapshot()
  })
})
